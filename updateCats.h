///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    updateCats.h
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
////////////////////////////////////////////////////////////////////////////
#pragma once
#include "catDatabase.h"

extern void updateCatName(int index, char newName[]);
extern void fixCat(int index);
extern void updateCatWeight(int index, float newWeight);
extern void updateCollarColor1(int index, enum Color newCollarColor1);
extern void updateCollarColor2(int index, enum Color newCollarColor2);
extern void updateCatLicense(int index, unsigned long long newLicense);

