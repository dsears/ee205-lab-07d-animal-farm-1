///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    deleteCats.c
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
///////////////////////////////////////////////////////////////////////////
#include<string.h>
#include "catDatabase.h"
#include "deleteCats.h"
#include "config.h"

void deleteAllCats()
{
   //memset(catDeetsArray[], 0, MAX_CATS);
   for(int i = 0; i < MAX_CATS; i++)
   {
    memset(catDeetsArray[i].Name, 0, MAX_CATS);

   catDeetsArray[i].Gender = 0;
   catDeetsArray[i].weight = 0;
   catDeetsArray[i].Breed = 0;
   catDeetsArray[i].isFixed = 0;
   catDeetsArray[i].collarColor1 = 0;
   catDeetsArray[i].collarColor2 = 0;
   catDeetsArray[i].license = 0;
   
   }
   currentNumCats = 0;
}
