///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    config.h
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    02_MAR_2022
///
//////////////////////////////////////////////////////////////////////////////
#pragma once

#define PROGRAM_NAME "animalFarm"
