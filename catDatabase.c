///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    catDatabase.c
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
////////////////////////////////////////////////////////////////////////////
#include<stdbool.h>
#include "addCats.h"
#include "catDatabase.h"
#include "config.h"

//#define USE_STRUCT

//#define MAX_CATS 30

struct catDeets catDeetsArray[MAX_CATS];
//bool isFixed[MAX_CATS];
//float weight[MAX_CATS];
int currentNumCats;
//enum gender Gender[MAX_CATS];
//enum breed Breed[MAX_CATS];
//char Name[MAX_CATS][MAX_NAME];
//char name[MAX_CATS][MAX_NAME];

